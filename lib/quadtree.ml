let max_depth = 10

let iter_pairs l ~f =
  let rec loop l f elt =
    List.iter (f elt) l;
    match l with
    | [] -> ()
    | h :: t -> loop t f h in
  match l with
  | [] -> ()
  | h :: t -> loop t f h

type 'a elt = {
  mutable elt_x: int; mutable elt_y: int;
  elt_dim: int * int;
  element: 'a;
  mutable parents: 'a leaf list;
  root: 'a t
} and 'a leaf = {
  mutable elts: 'a elt list
}
and 'a quad = {
  mutable nw: 'a tree;
  mutable ne: 'a tree;
  mutable se: 'a tree;
  mutable sw: 'a tree;
}  
and 'a tree =
  | Quad of 'a quad
  | Leaf of 'a leaf     
and 'a t = {
  x: int; y: int; w: int; h: int;
  mutable modification_allowed: bool;
  mutable tree: 'a tree
}

let root t = t.root

let value t = t.element

let pos t = (t.elt_x,t.elt_y)

let bbox t = (t.elt_x,t.elt_y,fst t.elt_dim,snd t.elt_dim)

let fresh_leaf elts : 'a tree =
  let result : 'a leaf = {elts=elts} in
  Leaf result

let create ~x ~y ~w ~h elts =
  let tree = fresh_leaf elts in
 {x;y;w;h;tree; modification_allowed=true}

let nw (x,y,w,h) =
  let w1 = w - w / 2 in
  let h1 = h - h / 2 in
  x,y, h1, w1

let ne (x,y, w,h) =
  let w2 = w / 2 in
  let w1 = w - w2 in
  let h1 = h - h / 2 in
  x + w1,y, h1, w2

let sw (x,y, w,h) =
  let w1 = w - w/2 in
  let h2 = h / 2 in
  let h1 = h - h2 in
  x,y + h1, h2, w1

let se (x,y, w,h) =
  let w2 = w/2 in
  let w1 = w - w2 in
  let h2 = h / 2 in
  let h1 = h - h2 in
  x + w1,y + h1, h2, w2

let intersects (x1,y1,w1,h1) (x2,y2,w2,h2) =
  not (x1+w1<x2 || x2+w2<x1 || y1+h1<y2 || y2+h2<y1)

let remove_node node elt =
  elt.parents <- List.filter (fun parent -> not (parent = node)) elt.parents

let intersects_node nw (elt: 'a elt) =
  intersects nw (elt.elt_x, elt.elt_y, (fst elt.elt_dim), (snd elt.elt_dim))

let intersects_node_pair (elt1: 'a elt) (elt2: 'a elt) =
  intersects (elt1.elt_x, elt1.elt_y, (fst elt1.elt_dim), (snd elt1.elt_dim)) (elt2.elt_x, elt2.elt_y, (fst elt2.elt_dim), (snd elt2.elt_dim))

let build_leaf elts =
  let node = {elts} in
  List.iter (fun elt -> elt.parents <- node :: elt.parents) elts;
  Leaf node

let rec insert_tree d v bp = function
  | (Leaf contents) when d < max_depth ->
    let elts = contents.elts in
    List.iter (remove_node contents) elts;
    let elts = (v :: elts) in
    let nw = build_leaf (List.filter (intersects_node (nw bp)) elts) in
    let ne = build_leaf (List.filter (intersects_node (ne bp)) elts) in
    let sw = build_leaf (List.filter (intersects_node (sw bp)) elts) in
    let se = build_leaf (List.filter (intersects_node (se bp)) elts) in
    Quad {nw;ne;sw;se}
  | ((Leaf contents) as node) ->
    contents.elts <- v :: contents.elts;
    v.parents <- contents :: v.parents;
    node
  | ((Quad (contents)) as node) ->
    if intersects_node (nw bp) v then begin
      contents.nw <- insert_tree (d + 1) v (nw bp) contents.nw;
    end;
    if intersects_node (ne bp) v then begin
      contents.ne <- insert_tree (d + 1) v (ne bp) contents.ne;
    end;
    if intersects_node (sw bp) v then begin
      contents.sw <- insert_tree (d + 1) v (sw bp) contents.sw;
    end;
    if intersects_node (se bp) v then begin
      contents.se <- insert_tree (d + 1) v (se bp) contents.se;
    end;
    node

let insert_elt elt qtree =
  qtree.tree <- insert_tree 0 elt (qtree.x,qtree.y,qtree.w,qtree.h) qtree.tree

let insert v ~x:elt_x ~y:elt_y ~w:elt_w ~h:elt_h qtree =
  assert (qtree.modification_allowed);
  let elt: 'a elt =
    { elt_x; elt_y; elt_dim=(elt_w, elt_h); element=v; parents=[]; root=qtree } in
  qtree.tree <- insert_tree 0 elt (qtree.x,qtree.y,qtree.w,qtree.h) qtree.tree;
  elt

let remove_elt (elt: 'a elt) =
  assert (elt.root.modification_allowed);
  List.iter
    (fun parent -> parent.elts <-
        List.filter
          (fun node -> not (node == elt))
          parent.elts)
    elt.parents;
  elt.parents <- []

let clamp ~min:(minimum: int) ~max:(maximum: int) vl =
  if vl < minimum then minimum
  else if maximum < vl
  then maximum
  else vl

let set_position_elt elt ~x ~y =
  let x = clamp ~min:elt.root.x ~max:(elt.root.x + elt.root.w - (fst elt.elt_dim)) x in
  let y = clamp ~min:elt.root.y ~max:(elt.root.y + elt.root.h - (snd elt.elt_dim)) y in
  remove_elt elt; elt.elt_x <- x; elt.elt_y <- y;
  insert_elt elt elt.root

let update_position_elt elt ~f =
  let x,y = f ~x:elt.elt_x ~y:elt.elt_y in
  let x = clamp ~min:elt.root.x ~max:(elt.root.x + elt.root.w - (fst elt.elt_dim)) x in
  let y = clamp ~min:elt.root.y ~max:(elt.root.y + elt.root.h - (snd elt.elt_dim)) y in
  remove_elt elt;
  elt.elt_x <- x; elt.elt_y <- y;
  insert_elt elt elt.root

let move_position_elt elt ~dx ~dy =
  let x,y = elt.elt_x + dx, elt.elt_y + dy in
  let x = clamp ~min:elt.root.x ~max:(elt.root.x + elt.root.w - (fst elt.elt_dim)) x in
  let y = clamp ~min:elt.root.y ~max:(elt.root.y + elt.root.h - (snd elt.elt_dim)) y in
  remove_elt elt;
  elt.elt_x <- x; elt.elt_y <- y;
  insert_elt elt elt.root

let rec iter_colissions_tree f = function
  | Leaf contents ->
    iter_pairs ~f:(fun p1 p2 -> if not (p1 == p2) && intersects_node_pair p1 p2 then f p1 p2) contents.elts
  | Quad contents ->
    iter_colissions_tree f contents.nw;
    iter_colissions_tree f contents.ne;
    iter_colissions_tree f contents.sw;
    iter_colissions_tree f contents.se

let iter_colissions tree ~f =
  let old_modif_allowed = tree.modification_allowed in
  iter_colissions_tree f tree.tree;
  tree.modification_allowed <- old_modif_allowed


let iter_colissions_elt elt ~f =
  let old_modif_allowed = elt.root.modification_allowed in
  elt.root.modification_allowed <- false;
  List.iter  (fun contents ->
    List.iter (fun other ->
      if not (elt == other) && intersects_node_pair elt other
      then f other
    ) contents.elts
  )  elt.parents;
  elt.root.modification_allowed <- old_modif_allowed

let rec iter_intersects_tree f tree_box bbox (tree: 'a tree) =
  match tree with
  | Quad contents ->
    if intersects (nw tree_box) bbox then
      iter_intersects_tree f (nw tree_box) bbox contents.nw;
    if intersects (ne tree_box) bbox then
      iter_intersects_tree f (ne tree_box) bbox contents.ne;
    if intersects (sw tree_box) bbox then
      iter_intersects_tree f (sw tree_box) bbox contents.sw;
    if intersects (se tree_box) bbox then
      iter_intersects_tree f (se tree_box) bbox contents.se
  | Leaf contents ->
    List.iter (fun elt -> if intersects_node bbox elt then f elt) contents.elts

let iter_intersects (qtree: 'a t) ~x  ~y ~w ~h ~f : unit =
  iter_intersects_tree f (qtree.x,qtree.y,qtree.w,qtree.h) (x,y,w,h) qtree.tree

let try_set_position_elt ~pred:predicate elt ~x ~y =
  let x = clamp ~min:elt.root.x ~max:(elt.root.x + elt.root.w - (fst elt.elt_dim)) x in
  let y = clamp ~min:elt.root.y ~max:(elt.root.y + elt.root.h - (snd elt.elt_dim)) y in
  let collides = ref false in
  iter_intersects elt.root ~x ~y ~w:(fst elt.elt_dim) ~h:(snd elt.elt_dim) ~f:(fun oelt ->
    collides := !collides || (predicate oelt && not (elt == oelt))
  );
  if not !collides then begin
    remove_elt elt; elt.elt_x <- x; elt.elt_y <- y;
    insert_elt elt elt.root;
    true
  end else false

let rec iter_leaves_internal f tree_box (tree: 'a tree) =
  match tree with
  | Quad contents ->
    iter_leaves_internal f (nw tree_box) contents.nw;
    iter_leaves_internal f (ne tree_box) contents.ne;
    iter_leaves_internal f (sw tree_box) contents.sw;
    iter_leaves_internal f (se tree_box) contents.se
  | Leaf contents ->
    f tree_box contents.elts

let iter_leaves (qtree: 'a t) ~f =
  iter_leaves_internal f (qtree.x,qtree.y,qtree.w,qtree.h) qtree.tree
