Set Implicit Arguments.

From CFML Require Import WPLib Stdlib.
From TLC Require Import LibListZ.
Generalizable Variables A.

Implicit Types n m: int.
Implicit Types p q : loc.

From Proofs Require Import Quadtree.

Definition position : Type := int * int * int * int.

Definition px '((x, _, _, _) : position) := x.
Definition py '((_, y, _, _) : position) := y.
Definition pw '((_, _, w, _) : position) := w.
Definition ph '((_, _, _, h) : position) := h.

Definition intersects '((x1,y1,w1,h1) : position) '((x2,y2,w2,h2): position) : bool :=
  negb ((x1+w1 <? x2) || (x2+w2<?x1) || (y1+h1<?y2) || (y2+h2<?y1)).

Definition valid_area '((_,_, w,h): position) : bool :=
  (w >=? 0) && (h >=? 0).

Definition nwZ '((x,y,w,h): position) :=
  let w1 := w - Z.div2 w in
  let h1 := h - Z.div2 h in
  (x,y, h1, w1).

Definition neZ '((x,y, w,h): position) :=
  let w2 := Z.div2 w in
  let w1 := w - w2 in
  let h1 := h - Z.div2 h in
  (x + w1,y, h1, w2).

Definition swZ '((x,y, w,h): position) :=
  let w1 := w - Z.div2 w in
  let h2 := Z.div2 h in
  let h1 := h - h2 in
  (x,y + h1, h2, w1).

Definition seZ '((x,y, w,h): position) :=
  let w2 := Z.div2 w in
  let w1 := w - w2 in
  let h2 := Z.div2 h in
  let h1 := h - h2 in
  (x + w1,y + h1, h2, w2).

Definition Elt A `{EA: Enc A}
               (root: loc) (parents: list loc)
               (vl: A) (area: position)
          (r: loc) : hprop :=
  \[ valid_area area] \*
  r ~~~> `{ elt_x' := px area; elt_y' := py area; elt_dim' := (pw area,ph area);
            element' := vl; root' := root; parents' := parents }.

Definition leaf A `{EA: Enc A} (root: loc) 
  (elts: list (loc * list loc * (A * position))) 
  (r: loc) :=
  (hfold_list (fun '((elt, parents,(vl,dims))) =>
                 elt ~~> Elt root parents vl dims) elts) \-*
  r ~~~> `{
      elts' := List.map (fun '(elt, _, _) => elt) elts
  }.
  
Inductive logical_tree A :=
| LLeaf (elts: list (loc * list loc * (A * position))) : logical_tree A
| LQuad
    (nw: logical_tree A)
    (ne: logical_tree A)
    (sw: logical_tree A)
    (se: logical_tree A) : logical_tree A.

Fixpoint valid_logical_tree A (tree: logical_tree A) (area: position) : bool :=
  match tree with
  | LLeaf elts => List.forallb (fun '(_, _, (_, position)) => intersects position area) elts
  | LQuad nw ne sw se =>
      valid_logical_tree nw (nwZ area) &&
      valid_logical_tree ne (neZ area) &&
      valid_logical_tree sw (swZ area) &&
        valid_logical_tree se (neZ area)
  end.

Fixpoint logical_tree_elts A (tree: logical_tree A) : list (loc * list loc * (A * position)) :=
  match tree with
  | LLeaf elts => elts
  | LQuad nw ne sw se =>
      logical_tree_elts nw ++ logical_tree_elts ne ++ logical_tree_elts sw ++ logical_tree_elts se
  end.

Fixpoint logical_tree_insert A (tree: logical_tree A) (area: position) elt :=
  let '(_, _, (_, pos)) := elt in
  match tree with
  | LLeaf elts =>
      if intersects area pos
      then LLeaf (elt :: elts)
      else LLeaf elts
  | LQuad nw ne sw se =>
      LQuad (logical_tree_insert nw (nwZ area) elt)
            (logical_tree_insert ne (neZ area) elt)
            (logical_tree_insert sw (swZ area) elt)
            (logical_tree_insert se (seZ area) elt)
  end.
        
Fixpoint logical_tree_remove A (tree: logical_tree A) (ind: loc) :=
  match tree with
  | LLeaf elts =>
      LLeaf (List.filter (fun '(ind', _, _) => negb (Nat.eqb ind ind')) elts)
  | LQuad nw ne sw se =>
      LQuad (logical_tree_remove nw ind)
            (logical_tree_remove ne ind)
            (logical_tree_remove sw ind)
            (logical_tree_remove se ind)
  end.

Fixpoint tree A `{EA: Enc A}
  (root: loc) (area: position)
  (model: logical_tree A) (r: tree_ A) :=
  \[valid_logical_tree model area] \*
  match model with 
  | LLeaf elts =>
      \exists r_loc,
          \[ r = Leaf r_loc] \*
            \[List.Forall (fun '((_, parents, _)) =>
                             List.Exists
                               (fun parent => r_loc = parent) parents)
                elts] \*
            r_loc ~> leaf root elts
  | LQuad m_nw m_ne m_sw m_se => 
      \exists (r_loc: loc) (p_nw p_ne p_se p_sw: tree_ A),
      \[r = Quad r_loc] \*
      r_loc ~~~> `{
         nw' := p_nw;
         ne' := p_ne;
         se' := p_se;
         sw' := p_sw
      } \*
        p_nw ~> tree root (nwZ area) m_nw \*
        p_ne ~> tree root (neZ area) m_ne \*
        p_se ~> tree root (seZ area) m_se \*
        p_sw ~> tree root (swZ area) m_sw
  end.

  
Definition quadtree
  A `{EA: Enc A}
  (elts: list (A * position))
  (area: position) (modification_allowed: bool)
  (r: loc) :=
  \exists (parents: list (loc * list loc)) 
          (model: logical_tree A) 
          (tree_data: tree_ A),
      (hfold_list (fun '((elt, parents,(vl,dims))) =>
                       elt ~~> Elt r parents vl dims) (List.combine parents elts)) \*
      r ~~~> `{
         x' := px area; y' := py area;
         w' := pw area; h' := ph area;
         tree' := tree_data;
         modification_allowed' := modification_allowed
      } \* tree_data ~> tree r area model.


Lemma fresh_leaf_spec : forall A `{EA: Enc A},
    SPEC_PURE (fresh_leaf (@nil (elt_ A)))
      POST (fun (r: tree_ A) => \exists r_loc, \[r = Leaf r_loc] \*
                                      r_loc ~~~> `{ elts' := (nil : list loc) } ).
Proof.
  intros A EA.
  xcf; eauto.
  xapp;=>r.
  xvals*.
Qed.

Lemma create_spec : forall A `{EA:Enc A} (x: int) (y: int) (w: int) (h: int),
  SPEC (create x y w h (@nil (elt_ A)))
    PRE (\[(w >= 0) /\ (h >= 0)])
    POST (fun (r: loc) => r ~> quadtree (nil: list (A * position)) (x,y,w,h) true).
Proof using.
  intros; unfold quadtree.
  xcf; eauto.
  xpull;=> [Hw Hh].
  xapp (fresh_leaf_spec); eauto;=> leaf leaf_loc  Hleaf.
  xapp;=> r.
  rewrite !(@repr_eq _ (fun _ => \exists _, _) r ); simpl.
  xsimpl*.
  instantiate (1:=LLeaf nil); instantiate (1:=(@nil  (loc * list loc))); simpl.
  rewrite (@repr_eq _ _ leaf).
  unfold VerifyQuadtree.leaf.
  xsimpl*.
  rewrite hfold_list_nil.
  rewrite (repr_eq (fun _ => \[] \-* _) leaf_loc), hwand_hempty_l.
  xsimpl*.
Qed.
